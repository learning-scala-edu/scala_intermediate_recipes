import Dependencies._

ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "farm"
ThisBuild / organizationName := "farm"

lazy val root = (project in file("."))
  .settings(
    name := "Animals",
    libraryDependencies += "org.specs2" %% "specs2-core" % "4.5.1"
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
