package farm

class Farm(name: String) {
  def tasksForTheDay(animals: Seq[Animal]): Seq[FarmTask] = {
    animals.flatMap {
      case animal @ Animal(_, age, Cow) =>
        Seq(FarmTask(animal, "milking", age))
      
      case animal @ Animal(_, age, Chicken) =>
        Seq(FarmTask(animal, "checking for eggs", age))

      case animal @ Animal(_, age, Horse) =>
        Seq(FarmTask(animal, "plowing", age))

      case _ @ Animal(_, _, Wolf) =>
        throw new Exception("Wolves are attacking the farm!")

      case _ =>
        Seq()
    }
  }
}
